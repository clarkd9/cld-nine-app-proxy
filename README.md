# cld-nine-app-proxy

NGINX proxy app for cld-nine app.

## Usage

### Environment Variables 

* 'LISTEN_PORT' - port to listen on (default: '8000')
* 'APP_HOST' - Hostname of the APP to forward requests to (default: 'app')
* 'APP_PORT' - Port of the APP to forward requests to (default: '9000')
